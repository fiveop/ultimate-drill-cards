# Ultimate Drill Cards

The Ultimate Drill Cards provides descriptions of Ultimate practice drills. The
drill descriptions are provided as [XeTeX][xetex] source, which compiles into A4
sized pages, one per drill.

## Contributing

Contributions are welcome in most forms, with most contents and of all sizes.

Create an issue or a pull request at the [Gitlab repository][gl-repo].

Offer critique, raise questions, translate existing content or contribute new
content.

Fixing a typo is equally welcome as contributing a whole new card.

Note that if you want to contribute by way of editing existing or providing new
content, you have to agree to publish it under the CC-BY-SA license (see below).

## Build

Run `build.sh` to create PDF files from the drill `.tex` files. [XeTeX][xetex]
is required for the build.

## License

Copyright 2016, Contributors to Ultimate Drill Cards 2016

All contents of this repository are made available under the Creative Commons
Attribution-ShareAlike 4.0 International Public License.

See the file [LICENSE](LICENSE) for the full text of the license.

[gl-repo]: http://gitlab.com/fiveop/ultimate-drill-cards
[xetex]: http://xetex.sourceforge.net/
