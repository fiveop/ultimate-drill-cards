Ultimate Drill Cards Style Guide
================================

Ultimate Drill Cards should use consistent terminology. In order to achieve this goal, only one word must be used for every concept or thing. The glossary, precedent and good judgment define the rule, in that order. New or changed glossary entries might change the rule.

Ultimate Drill Cards use SI units. For the relevant measures of length and time, these are meter and second, respectively.

## Material

The material section is always a list. Predefined LaTeX commands for the different materials (discs and cones) must be used.

## Setup

The setup sections are written in the imperative.

They describe how to setup the initial static situation, from which the drill starts. Initial positions of players, discs and cones are given, as well as the facing direction of the players.

Thrower, marker and receiver roles are designated.

Descriptions of actions have no place in the setup sections. They belong into the execution sections.
