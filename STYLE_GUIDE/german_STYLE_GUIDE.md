Ultimate Drill Cards Style Guide
================================

Ultimate Drill Cards sollen unter Verwendung von konsistenter  Terminologie geschrieben werden. Um das zu erreichen, muss für jedes Konzept oder jedes Ding nur ein Wort verwendet werden. Das Glossar, bestehende Verwendung und gesunder Menschenverstand definieren in dieser Reihenfolge die Regel. Neue oder geänderte Einträge des Glossars können die Regel ändern.

Ultimate Drill Cards verwenden SI Einheiten. Für die relevanten Größen Länge und Zeit sind das Meter und Sekunden.

Personenbezeichnungen haben im Text einer Karte durchgängig das selbe Geschlecht. Die Wahl obliegt dem initialen Autor der Karte.

## Material

Der "Material"-Abschnitt ist immer eine Liste. Für die verschiedenen Materialien (Scheiben und Hütchen) müssen vordefinierte LaTeX-Befehle verwendet werden.

## Aufbau

Der "Aufbau"-Abschnitt wird im Imperativ geschrieben.

Er beschreibt die initiale, statische Situation bevor die Übung beginnt. Initiale Positionen der Spieler, Scheiben und Hütchen werden beschrieben, sowie die Blickrichtung der Spieler.

Auf Werfer, Marker und Fänger Rollen wird verwiesen.

Beschreibungen von Aktionen gehören nicht in den "Aufbau"-Abschnitt, sondern in den "Durchführung"-Abschnitt.
