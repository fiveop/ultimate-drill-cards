#!/bin/sh

# Copyright 2016, Contributors to Ultimate Drill Cards 2016
# Made available under the Creative Commons Attribution-ShareAlike 4.0
# International Public License
#
# See the file LICENSE for the full text of the license.

# Parameters
PAPERSIZE=a4paper
LANGUAGE=ngerman

# Constants
OUTDIR=output

SOURCE_FILE_NAME="drills.tex"
SOURCE_FILE="$OUTDIR/$SOURCE_FILE_NAME"

FRAME_DIR=frame
HEADER="$FRAME_DIR/header.tex"
FOOTER="$FRAME_DIR/footer.tex"

STY_FILE="drills.sty"
LOCALIZATION_DIR="localization"

prepare_output_directory() {
    mkdir -p "$OUTDIR/"
    cp "$STY_FILE" "$OUTDIR/"
    cp -r "$LOCALIZATION_DIR" "$OUTDIR/"
}

add_drills_to_source_file() {
    cat `find drills -regextype sed -regex "drills\/drill[0-9]*\.tex" | sort -t l -k 5 -g` >> "$SOURCE_FILE"
}

create_source_file() {
    cat "$HEADER" | sed -e "s/PAPERSIZE/$PAPERSIZE/g" | sed -e "s/LANGUAGE/$LANGUAGE/g" > "$SOURCE_FILE"
    add_drills_to_source_file
    cat "$FOOTER" >> "$SOURCE_FILE"
}

run_tex() {
    cd "$OUTDIR/"
    xelatex "$SOURCE_FILE_NAME"
    xelatex "$SOURCE_FILE_NAME"
    cd ..
}

build() {
    prepare_output_directory
    create_source_file
    run_tex
}

build
